package DBM;

use strict;
use warnings;

use base 'Class::Singleton';

use Conf;
use DBI;


sub _new_instance {
    my $class = shift;
    my $db_conf = Conf::val("DB") // die "Config doesn't contain db data";
    my $dbh = DBI->connect(
        "DBI:mysql:database=$db_conf->{base};host=$db_conf->{host};port=$db_conf->{port}",
        $db_conf->{user},
        $db_conf->{password},
        {RaiseError => 1}
    ) // die "Can't connect to DB: $!";
    $dbh->{mysql_enable_utf8} = 1;
    return bless { dbh => $dbh }, $class;
}

sub new {
    my $class = shift;
    return $class->instance();
}

sub last_id {
    my $self = shift;
    return $self->{dbh}->{mysql_insertid};
}

sub query {
    my $self = shift;
    my ($query, @params) = @_;

    my $sth = $self->{dbh}->prepare($query);
    if ( !@params || (@params && ref $params[0] ne 'ARRAY') ) {
        $sth->execute(@params);
    } else {
        $sth->execute(@$_) for (@params);
    }

    return $sth;
}


1;

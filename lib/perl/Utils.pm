package Utils;

use strict;
use warnings;

use JSON::XS;
use POSIX;
use utf8;
use Encode;


sub sid {
    return rand_str(64);
}

sub availible_date {
    my $time = time;
    return [
        map {
            strftime("%d-%m-%Y", localtime($time + ($_ * 60 * 60 * 24)))
        } (1..3)
    ];
}

sub availible_time {
    return [
        "9:00 - 12:00",
        "12:00 - 16:00",
        "16:00 - 19:00",
    ];
}

sub availible_status {
    return [
        'new',
        'accepted',
        'stored',
        'in_progress',
        'done',
    ];
}

sub make_selected {
    my ($list, $selected) = @_;
    return [
        map {
            my $hash = { val => $_ };
            $hash->{selected} = 1 if ($_ eq $selected);
            $hash;
        } @$list
    ];
}


sub get_params {
    my $q = CGI->new;
    my $params = $q->Vars;
    for (keys %$params) {
        utf8::decode($params->{$_}) if utf8::is_utf8($params->{$_});
    }
    return $params;
}

sub get_json_params {
    my $q = CGI->new;
    return undef unless ($q->request_method eq 'POST');
    my $data = $q->param('POSTDATA');
    $data = Encode::encode_utf8($data) if utf8::is_utf8($data);

    my $decoded_json;
    eval { $decoded_json = JSON::XS::decode_json($data) };
    return $decoded_json;
}

sub get_price {
    my ($card) = @_;
    # TODO логика
    return 1000;
}

sub trunc {
    no warnings;
    return (map {
        s/(^[\s\r\n]+|[\s\r\n]+$)//g if $_; $_;
    } @_);
}

sub rand_str {
    my ($len) = @_;
    my @chars = ('a'..'z','A'..'Z','0'..'9','_');
    my $random_string;
    $random_string .= $chars[rand @chars] for (1..$len); 
    return $random_string;
}

sub deep_trunc {
    my ($data) = @_;

    my $type = ref $data;
    if ($type eq '') {
        return trunc $data;
    }

    if ($type eq 'ARRAY') {
        return [ map { deep_trunc($_) } @$data ];
    }

    if ($type eq 'HASH') {
         return { map { $_ => deep_trunc($data->{$_}) } keys %$data };
    }

    return;
}



1;
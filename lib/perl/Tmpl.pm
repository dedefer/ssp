package Tmpl;

use strict;
use warnings;

use base 'Class::Singleton';

use utf8;

use Conf;
use Text::Caml;
use CGI;
use CGI::Cookie;

my %RESPONSE_STATUS = (
    100 => "100 Continue",
    101 => "101 Switching Protocols",
    102 => "102 Processing",
    200 => "200 OK",
    201 => "201 Created",
    202 => "202 Accepted",
    203 => "203 Non-Authoritative Information",
    204 => "204 No Content",
    205 => "205 Reset Content",
    206 => "206 Partial Content",
    207 => "207 Multi-Status",
    208 => "208 Already Reported",
    226 => "226 IM Used",
    300 => "300 Multiple Choices",
    301 => "301 Moved Permanently",
    302 => "302 Moved Temporarily",
    303 => "303 See Other",
    304 => "304 Not Modified",
    305 => "305 Use Proxy",
    307 => "307 Temporary Redirect",
    308 => "308 Permanent Redirect",
    400 => "400 Bad Request",
    401 => "401 Unauthorized",
    402 => "402 Payment Required",
    403 => "403 Forbidden",
    404 => "404 Not Found",
    405 => "405 Method Not Allowed",
    406 => "406 Not Acceptable",
    407 => "407 Proxy Authentication Required",
    408 => "408 Request Timeout",
    409 => "409 Conflict",
    410 => "410 Gone",
    411 => "411 Length Required",
    412 => "412 Precondition Failed",
    413 => "413 Payload Too Large",
    414 => "414 URI Too Long",
    415 => "415 Unsupported Media Type",
    416 => "416 Range Not Satisfiable",
    417 => "417 Expectation Failed",
    418 => "418 I’m a teapot",
    419 => "419 Authentication Timeout",
    421 => "421 Misdirected Request",
    422 => "422 Unprocessable Entity",
    423 => "423 Locked",
    424 => "424 Failed Dependency",
    426 => "426 Upgrade Required",
    428 => "428 Precondition Required",
    429 => "429 Too Many Requests",
    431 => "431 Request Header Fields Too Large",
    449 => "449 Retry With",
    451 => "451 Unavailable For Legal Reasons",
    499 => "499 Client Closed Request",
    500 => "500 Internal Server Error",
    501 => "501 Not Implemented",
    502 => "502 Bad Gateway",
    503 => "503 Service Unavailable",
    504 => "504 Gateway Timeout",
    505 => "505 HTTP Version Not Supported",
    506 => "506 Variant Also Negotiates",
    507 => "507 Insufficient Storage",
    508 => "508 Loop Detected",
    509 => "509 Bandwidth Limit Exceeded",
    510 => "510 Not Extended",
    511 => "511 Network Authentication Required",
    520 => "520 Unknown Error",
    521 => "521 Web Server Is Down",
    522 => "522 Connection Timed Out",
    523 => "523 Origin Is Unreachable",
    524 => "524 A Timeout Occurred",
    525 => "525 SSL Handshake Failed",
    526 => "526 Invalid SSL Certificate",

);

sub _new_instance {
    my $class = shift;
    warn "Open templates\n";
    my $engine = Text::Caml->new(default_partial_extension => 'mustache_ext');
    $engine->set_templates_path(Conf::root.'/templates');
    return bless { engine => $engine, ext => 'mustache', cookies => [] }, $class;
}

sub new {
    my $class = shift;
    return $class->instance();
}

sub add_cookie {
    my $self = shift;
    my ($cookie) = @_;
    my $c = CGI::Cookie->new(%$cookie);
    push @{$self->{cookies}}, $c;
    return; 
}

sub print_headers {
    my $self = shift;
    my ($code, %params) = @_;
    $code = $RESPONSE_STATUS{$code} // $code;
    $params{add_headers} //= {};
    $params{type} //= 'text/html';

    my $cgi = CGI->new;
    print $cgi->header(
        -type => $params{type},
        -status => $code,
        -charset => 'utf-8',
        -cookie => $self->{cookies},
        %{$params{add_headers}},
    );
    $self->{cookies} = [];
}

sub json {
    my $self = shift;
    my ($code, $data) = @_;
    $code = $RESPONSE_STATUS{$code} // $code;

    $self->print_headers($code, 'type' => 'application/json');

    my $encoded_json;
    eval { $encoded_json = JSON::XS::encode_json($data) };
    print $encoded_json if $encoded_json;
}

sub redirect {
    my $self = shift;
    my ($code, $redirect_to) = @_;
    $code = $RESPONSE_STATUS{$code} // $code;

    my $cgi = CGI->new;
    print $cgi->redirect(
        -uri => $redirect_to,
        -status => $code,
        -charset => 'utf-8',
        -cookie => $self->{cookies},
    );
    $self->{cookies} = [];
    return;
}

sub expand {
    my $self = shift;
    my ($code, $tmpl, $params) = @_;

    $tmpl .= ".$self->{ext}";
    $params //= {};


    $self->print_headers($code);

    my $cgi = CGI->new;

    print $self->{engine}->render_file($tmpl, $params);
    return;
}


1;
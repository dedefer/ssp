package Auth::Middleware;

use strict;
use warnings;

use Auth;
use Cache;
use Tmpl;
use Consts;


sub auth_check {
    my ($func) = @_;
    my $ret = sub {
        my ($context) = @_;
        $context //= {};

        unless ($context->{auth}) {
            my $mc = Cache->new;
            if (my $sid = Auth::extract_auth_cookie) {

                my $cache_uid = $mc->get("sid:$sid:uid");
                my $cache_flags = $mc->get("sid:$sid:flags");
                my $cache_login = $mc->get("sid:$sid:login");

                if ($cache_uid) {
                    unless (Auth::page_lock_manage($cache_uid)) {
                        Tmpl->new->redirect(303, '/index');
                        return;
                    };
                    $context->{auth} = {
                        login   => $cache_login,
                        sid     => $sid,
                        uid     => $cache_uid,
                        flags   => $cache_flags,
                        admin   => Auth::check_admin($cache_flags),
                    };
                } else {
                    Auth::delete_auth_cookie;
                }
            }
        }
        $func->($context);
    };
    return $ret;
}

sub auth_required {
    my ($func) = @_;
    my $ret = auth_check(sub {
        my ($context) = @_;
        unless ($context->{auth}) {
            # Tmpl->new->expand(401, 'unauthorized', $context);
            Tmpl->new->redirect(303, '/register');
            return;
        }
        $func->($context);
    });
    return $ret;
}

sub grants_required {
    my @grants = @_;

    my $middleware = sub {
        my ($func) = @_;
        my $ret = auth_required(sub {
            my ($context) = @_;
            my $flags = $context->{auth}->{flags};
            my $admin = $context->{auth}->{admin};

            my $required_flags = 0;
            $required_flags |= $Consts::flags{$_} for (@grants);
            unless (($flags & $required_flags) || $admin) {
                Tmpl->new->expand(403, 'forbidden', $context);
                return;
            }
            $func->($context);
        });
        return $ret;
    };
    return $middleware;
}

1;
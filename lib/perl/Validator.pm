package Validator;

use strict;
use warnings;

sub _validate_item {
    my ($s, $val) = @_;

    # use Data::Dumper;
    # # say Dumper $s;

    return "$s->{name} required"
        if ($s->{required} && (!defined $val));

    return '' if (!$s->{required} && (!defined $val));

    return '' unless (defined $s->{type});



    if ($s->{type} eq 'ENUM') {
        # say 'ENUM TEST';
        return "$s->{name} must be one of [ " . join(', ', @{$s->{value}}) ." ]"
            unless (ref $val eq '' && (grep { "$val" eq "$_" } @{$s->{value}}));
        # say 'ENUM TEST complete';

    } elsif ($s->{type} eq 'ARRAY') {

        # say 'ARRAY TEST';
        return "$s->{name} must be ARRAY"
            unless(ref($val) eq 'ARRAY');
        my $result = '';
        for my $item (@$val) {
            $result = _validate_item(
                { name => "$s->{name}\[\]", %{$s->{value}} },
                $item,
            );
            last if ($result);
        }
        return $result if ($result);
        # say 'ARRAY TEST complete';

    } elsif ($s->{type} eq 'HASH') {

        # say 'HASH TEST';
        return "$s->{name} must be HASH"
            unless(ref($val) eq 'HASH');
        my $result = '';
        for my $k (keys %{$s->{value}}) {
            $result = _validate_item(
                { name => "$s->{name}.$k", %{$s->{value}->{$k}} },
                $val->{$k}
            );
            last if ($result);
        }
        return $result if ($result);
        # say 'HASH TEST complete';

    } elsif ($s->{type} eq 'MATCH') {

        # say 'MATCH TEST';
        no warnings;
        return "$s->{name} must match $s->{value}"
            unless (ref $val eq '' && "$val" =~ $s->{value});
        # say 'MATCH TEST complete';

    } elsif ($s->{type} eq 'NOT_MATCH') {

        # say 'MATCH TEST';
        no warnings;
        return "$s->{name} must not match $s->{value}"
            unless (ref $val eq '' && "$val" !~ $s->{value});
        # say 'MATCH TEST complete';

    } elsif ($s->{type} eq 'BOOL') {

        return "$s->{name} must be @{[ !!$s->{value} ? 'true' : 'false' ]}"
            if (exists $s->{value} && !!$s->{value} ne !!$val);

    }

    return '';
}

sub validate {
    my ($schema, $data) = @_;

    return _validate_item(
        { name => '', %$schema },
        $data,
    );
}


1;
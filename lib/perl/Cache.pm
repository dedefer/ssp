package Cache;

use strict;
use warnings;

use base 'Class::Singleton';


use Conf;
use Cache::Memcached::Fast;


sub _new_instance {
    my $class = shift;
    my $mc_conf = Conf::val("memcached") // die "Config doesn't contain memcached data";
    my $memcached = Cache::Memcached::Fast->new({ 
        servers => $mc_conf->{servers},
        utf8 => 1,
        connect_timeout => 0.2,
        namespace => "tedb:",
    });
    return bless {
        cache => $memcached,
        default_expire => $mc_conf->{default_expire},
    }, $class;
}

sub new {
    my $class = shift;
    return $class->instance();
}

sub set {
    my $self = shift;
    my ($key, $value, $expire) = @_;
    $expire //= $self->{default_expire};

    $self->{cache}->set($key, $value, $expire);
    return;
}

sub delete {
    my $self = shift;
    my ($key) = @_;

    $self->{cache}->delete($key);
    return;
}

sub get {
    my $self = shift;
    my ($key) = @_;

    return $self->{cache}->get($key);
}

sub touch {
    my $self = shift;
    my ($key, $expire) = @_;
    $expire //= $self->{default_expire};

    $self->{cache}->touch($key, $expire);
    return;
}

1;
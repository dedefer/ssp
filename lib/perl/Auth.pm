package Auth;

use strict;
use warnings;

use Utils;
use Conf;
use Digest::MD5 qw(md5_hex);
use DBM;
use CGI::Cookie;
use Consts;
use Cache;

sub salt {
    return Utils::rand_str(32);
}

sub page_lock_manage {
    my ($uid) = @_;

    my $page = CGI->new->url(-relative => 1);
    my $ignored_pages = Conf::val('lock')->{_ignore} // [];

    # если страница в списке игнорируемых, то не считаем переходы по этим страницам
    unless (grep { $page eq $_ } @$ignored_pages) {

        unlock_prev_page($uid);

        Cache->new->set("user_page:$uid", $page); # обновляем предыдущую страниыу 

        return lock_page($uid, $page); # возвращаем удалось ли поставить лок

    }

    return 1;
}

sub unlock_prev_page {
    my ($uid) = @_;
    
    my $mc = Cache->new;
    # узнаем предыдущую страницу пользователя из кэша
    if (my $prev_page = $mc->get("user_page:$uid")) {
        # смотрим в конфиге есть ли локи по прошлой странице
        if (Conf::val('lock')->{$prev_page}) {
            # находим значение по которому на этом пользователе залочена эта страница
            if (my $lock_value = $mc->get("user_page:$uid:$prev_page")) {
                # если она таки залочена на НЕМ по этому значению то удаляем лок у страницы и пользователя
                if (my $locked_uid = $mc->get("lock:$prev_page:$lock_value")) {
                    if ($uid == $locked_uid) {
                        $mc->delete("lock:$prev_page:$lock_value");
                        $mc->delete("user_page:$uid:$prev_page");
                    }
                }
            }
        }
    }
}

sub lock_page {
    my ($uid, $page) = @_;
    # узнаем по какому параметру залочена эта страница
    if (my $lock_param = Conf::val('lock')->{$page}) {
        # узнаем значение параметра
        if (my $lock_value = Utils::get_params->{$lock_param}) {
            my $mc = Cache->new;
            # если страница не залочена по этому значению, то ставим лок на себя
            # и записываем значение лока к себе в кэш
            unless ($mc->get("lock:$page:$lock_value")) {
                $mc->set("lock:$page:$lock_value", $uid);
                $mc->set("user_page:$uid:$page",$lock_value);
                return 1;
            }
            return '';
        }
    }
    return 1;
}

sub check_credits {
    my ($login, $password) = @_;

    return '' unless ($login && $password);

    my $db = DBM->new;
    my $sth = $db->query('SELECT id, salt, password, flags FROM users WHERE login = ? LIMIT 1', $login);
    my $credits = $sth->fetchrow_hashref;
    $sth->finish;

    return '' unless $credits;

    my $db_password = $credits->{password};
    my $salt = $credits->{salt};

    $password = salted($password, $salt);

    return ($credits->{id}, $credits->{flags})
        if ($password eq $db_password);
    return '';
}

sub make_auth_cookie {
    my ($sid) = @_;
    my $expire = Conf::val('auth')->{expire};
    return {
        -name => 'X-AUTH-COOKIE',
        -expires => "+${expire}s",
        -value => $sid,
    };
}

sub delete_auth_cookie {
    Tmpl->new->add_cookie({
        -name => 'X-AUTH-COOKIE',
        -expires => "now",
        -value => "",
    });
}

sub extract_cookie {
    my ($name) = @_;
    my %cookie = CGI::Cookie->fetch;
    return $cookie{$name}->{value} if $cookie{$name};
}

sub extract_auth_cookie {
    my $auth_cookie = extract_cookie('X-AUTH-COOKIE');
    return undef unless $auth_cookie;
    return $auth_cookie->[0];
}

sub salted {
    my ($password, $salt) = @_;
    my $app_salt = Conf::val('app_salt');
    my $salted = md5_hex($app_salt . $password . $salt);
    return $salted;
}

sub user_exists {
    my ($login) = @_;
    my $db = DBM->new;
    my $sth = $db->query('SELECT login FROM users WHERE login = ? LIMIT 1', $login);
    my $check = !!$sth->fetchrow_hashref;
    $sth->finish;
    return $check;
}

sub register {
    my ($reg_info) = @_;

    my (
        $login, $password,
        $email, $phone,
        $name
    ) = @{$reg_info}{qw(
        login password
        email phone
        name
    )};

    return 0 if (user_exists($login));

    my $salt = salt;
    my $db = DBM->new;
    my $sth = $db->query(
        'INSERT INTO
        users(login, password, email, phone, salt, name)
        VALUES (?, ?, ?, ?, ?, ?)',
        $login, salted($password, $salt),
        $email, $phone,
        $salt, $name,
    );
    $sth->finish;

    return 1;
}

sub check_admin {
    my ($flags) = @_;
    return ($flags && ($flags & $Consts::flags{admin}));
}

sub sign_up {
    my ($login, $password) = @_;

    if (my ($uid, $flags) = check_credits($login, $password)) {
        my $sid = Utils::sid;

        my $mc = Cache->new;
        $mc->set("sid:$sid:uid", $uid);
        $mc->set("sid:$sid:flags", $flags);
        $mc->set("sid:$sid:login", $login);

        Tmpl->new->add_cookie(make_auth_cookie($sid));
    }
    return;
}

sub sign_out {
    my ($sid) = @_;

    my $mc = Cache->new;

    $mc->delete("sid:$sid:uid");
    $mc->delete("sid:$sid:flags");
    $mc->delete("sid:$sid:login");
    delete_auth_cookie;
}

1;
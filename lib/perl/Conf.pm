package Conf;

use strict;
use warnings;

use YAML ();

my $ROOT = "/opt/lampp";
my $conf = YAML::LoadFile("${ROOT}/etc/config.yml") // die "Can't read config file";

sub val {
    return $conf->{$_[0]};
}

sub root { $ROOT; }


1;
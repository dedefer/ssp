function admin_edit_item_btn_handler() {
    let row = $(this).parents('.item_row');
    let item_id = row.data().item_id;
    let name = row.find('.name').val();
    let description = row.find('.description').val();
    let weight = row.find('.weight').val();

    let d = {
        action: 'edit_items',
        items: [{
            item_id: item_id,
            name: name,
            description: description,
            weight: weight,
        }],
    };

    $.ajax({
        xhrFields: { withCredentials: true },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { alert('Данные изменены'); }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}
function admin_delete_item_btn_handler() {
    let row = $(this).parents('.item_row');
    let item_id = row.data().item_id;

    let d = {
        action: 'delete_items',
        items:[{
            item_id: item_id
        }]
    };

    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { 
                alert('Предмет удален');
                row.remove();
            }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}

function edit_item_btn_handler() {
    let row = $(this).parents('.item_row');
    let item_id = row.data().item_id;
    let name = row.find('.name').val();
    let description = row.find('.description').val();
    let weight = row.find('.weight').val();

    let d = {
        action: 'edit_items',
        items: [{
            item_id: item_id,
            name: name,
            description: description,
            weight: weight,
        }],
    };

    $.ajax({
        xhrFields: { withCredentials: true },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { alert('Данные изменены'); }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}
function delete_item_btn_handler() {
    let row = $(this).parents('.item_row');
    let item_id = row.data().item_id;

    let d = {
        action: 'delete_items',
        items:[{
            item_id: item_id
        }]
    };

    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { 
                alert('Предмет удален');
                row.remove();
            }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}

function admin_edit_card_btn_handler() {
    let card_id = $('#card_id').data().card_id;
    let delivery_address = $('.delivery_address').val();
    let income_address = $('.income_address').val();
    let income_time = $('.income_time').val();
    let income_date = $('.income_date').val();
    let delivery_time = $('.delivery_time').val();
    let delivery_date = $('.delivery_date').val();
    let status = $('.status').val();
    let price = $('.price').val();

    let d = {
        action: 'edit_card',
        card_id: card_id,
        delivery_address: delivery_address,
        income_address: income_address,
        income_time: income_time,
        income_date: income_date,
        delivery_time: delivery_time,
        delivery_date: delivery_date,
        status: status,
        price: price,
    };

    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { alert('Данные изменены'); }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}

function admin_delete_card_btn_handler() {
    let card_id = $('#card_id').data().card_id;

    let d = {
        action: 'delete_card',
        card_id: card_id
    };

    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { 
                alert('Заявка удалена');
                row.remove();
            }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}

$(window).on('load', function () {
    $('.admin_edit_item_btn').on('click', admin_edit_item_btn_handler);
    $('.admin_delete_item_btn').on('click', admin_delete_item_btn_handler);
    $('.admin_edit_card_btn').on('click', admin_edit_card_btn_handler);
    $('.admin_delete_card_btn').on('click', admin_delete_card_btn_handler);
});
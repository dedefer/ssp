function card_row_click_handle() {
    let href = $(this).data().href;
    window.location = href;
}

$(window).on('load', function () {
    $('.card_row').on('click', card_row_click_handle);
});
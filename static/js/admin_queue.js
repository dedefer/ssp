function queue_row_click_handle() {
    let href = $(this).data().href;
    window.location = href;
}

$(window).on('load', function () {
    $('.queue_row').on('click', queue_row_click_handle);
});
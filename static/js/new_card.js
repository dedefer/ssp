function create_card_btn_handler() {
    let delivery_address = $('#delivery_address').val();
    let income_address = $('#income_address').val();
    let income_time = $('#income_time').val();
    let income_date = $('#income_date').val();
    let delivery_time = $('#delivery_time').val();
    let delivery_date = $('#delivery_date').val();

    let d = {
        action: 'create_card',
        income_address: income_address,
        delivery_address: delivery_address,
        income_time: income_time,
        income_date: income_date,
        delivery_time: delivery_time,
        delivery_date: delivery_date,
    };

    let items = $('.item_row').map((_i, el) => {
        let item = {};
        let jel = $(el);
        item.name = jel.find('.name').val();
        item.description = jel.find('.description').val();
        item.weight = jel.find('.weight').val();

        item.name.trim();
        item.description.trim();
        item.weight.trim();

        if (item.name && item.weight) {
            return item;
        }
        return;
    }).get();

    d.items = items;

    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: '/card_api',
        dataType: 'json',
        data: JSON.stringify(d)
    }).then(
        (data) => {
            if (data.success) { alert('Заявка создана'); }
            else { alert('Ошибка: ' + data.error); }
        },
        (_ajax, error) => { alert('Ошибка: ' + error); }
    );
}

$(window).on('load', function () {
    $('#add_item_btn').on('click', function() {
        let item_row = $('.item_row').last();
        item_row.after(item_row.clone());
    });
    $('#create_card_btn').on('click', create_card_btn_handler);
});